#include <errno.h>
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <netdb.h>
#include <sys/socket.h>
#include <unistd.h>
#include <sys/types.h>
#include "cJSON.h"

//my struct use all time
typedef struct{
	int chipflag;// 1:straight 2:Right Bend 3:Left Bend 4:nearly straight
	int switchflag; //0:no 1:exist
	double length;
	double radius;
	double angle;
	int numtobend;//1,2,?c,9 straight num	0:now bend
	double disttobend;//distSUMtoBend
	int shouldswitch;//0:no 1:right 2:left
}chip;

typedef struct{
	int pieceIndex;
	double inPieceDistance;
}Position;

typedef struct{
	int lanenum;//lane num
	int lanedistfromcenter[10];//lane offset
	int piecenum;
	int turboflag;
	int turbonum;
	int onswitch; //1:on
	Position Oldpos;
	Position Nowpos;
	double Oldvelocity;
	double Nowvelocity;
	double OldAngle;
	double NowAngle;
	chip chips[200];
	int nowlap;
	int lapnum;
	int inif;
}mystatus;


static cJSON *ping_msg();
static cJSON *join_msg(char *bot_name, char *bot_key);
static cJSON *throttle_msg(double throttle);
static cJSON *make_msg(char *type, cJSON *msg);

static cJSON *read_msg(int fd);
static void write_msg(int fd, cJSON *msg);

static void error(char *fmt, ...)
{
    char buf[BUFSIZ];

    va_list ap;
    va_start(ap, fmt);
    vsnprintf(buf, BUFSIZ, fmt, ap);
    va_end(ap);

    if (errno)
        perror(buf);
    else
        fprintf(stderr, "%s\n", buf);

    exit(1);
}

static int connect_to(char *hostname, char *port)
{
    int status;
    int fd;
    char portstr[32];
    struct addrinfo hint;
    struct addrinfo *info;

    memset(&hint, 0, sizeof(struct addrinfo));
    hint.ai_family = PF_INET;
    hint.ai_socktype = SOCK_STREAM;

    sprintf(portstr, "%d", atoi(port));

    status = getaddrinfo(hostname, portstr, &hint, &info);
    if (status != 0) error("failed to get address: %s", gai_strerror(status));

    fd = socket(PF_INET, SOCK_STREAM, 0);
    if (fd < 0) error("failed to create socket");

    status = connect(fd, info->ai_addr, info->ai_addrlen);
    if (status < 0) error("failed to connect to server");

    freeaddrinfo(info);
    return fd;
}

static void log_message(char *msg_type_name, cJSON *msg)
{
    cJSON *msg_data;

    if (!strcmp("join", msg_type_name)) {
        puts("Joined");
    } else if (!strcmp("gameInit", msg_type_name)) {
        puts("game Init");
	puts(cJSON_Print(msg));
    } else if (!strcmp("gameStart", msg_type_name)) {
        puts("Race started");
    } else if (!strcmp("crash", msg_type_name)) {
        puts("Someone crashed");
    } else if (!strcmp("gameEnd", msg_type_name)) {
        puts("Race ended");
    } else if (!strcmp("error", msg_type_name)) {
        msg_data = cJSON_GetObjectItem(msg, "data");
        if (msg_data == NULL)
            puts("Unknown error");
        else
            printf("ERROR: %s\n", msg_data->valuestring);
    }
}

//my function
//CreateRce
cJSON *CreateRace(char *bot_name, char *bot_key,char *trackname,int carcount){
    cJSON *data = cJSON_CreateObject();
	cJSON *botId= cJSON_CreateObject();

	cJSON_AddStringToObject(botId, "name", bot_name);
    cJSON_AddStringToObject(botId, "key", bot_key);
	cJSON_AddItemToObject(data,"botId", botId);
	cJSON_AddStringToObject(data, "trackName", trackname);
	//cJSON_AddStringToObject(data, "password", "nothing");
	cJSON_AddNumberToObject(data, "carCount", carcount);

    return make_msg("joinRace", data);

}

cJSON *turbo_msg(){
    cJSON *data = cJSON_CreateObject();
	
	return make_msg("turbo",cJSON_CreateString("Power is Power"));
}
//Lane analyze switch
void switch_deside(mystatus* mydata){
	double length[20];
	int switchnum[20];
	int i;
	int k=0;
	length[0]=0;
	for(i=0;i<mydata->piecenum;i++){
		if(mydata->chips[i].switchflag==1){
			k++;
			length[k]=0;
			switchnum[k]=i;
		}
		if(1<mydata->chips[i].chipflag){
			length[k]+=mydata->chips[i].radius*M_PI*mydata->chips[i].angle/360.0;
		}
	}

	length[k]=length[k]+length[0];
	for(i=1;i<=k;i++){
		if(0<length[i]){
			mydata->chips[switchnum[i]].shouldswitch=1;
		}
		else if(0>length[i]){
			mydata->chips[switchnum[i]].shouldswitch=2;
		}else{
			mydata->chips[switchnum[i]].shouldswitch=0;
		}
	}

}

void stage_analyze(mystatus* mydata){
	int i;
	int k=0,n=0;
	double length=0;

	for(i=mydata->piecenum-1;0<=i;i--){
		if(mydata->chips[i].chipflag==2 || mydata->chips[i].chipflag==3){
			mydata->chips[i].disttobend=0;
			mydata->chips[i].numtobend=0;
			length=0;
			n=0;
		}else{//not bend
			n++;
			mydata->chips[i].numtobend=n;
			mydata->chips[i].disttobend=mydata->chips[i].length+length;
			length=mydata->chips[i].disttobend;
		}
	}

	for(i=mydata->piecenum-1;0<=i;i--){
		if(mydata->chips[i].chipflag==2 || mydata->chips[i].chipflag==3){
			break;
		}else{
			mydata->chips[i].numtobend=mydata->chips[i].numtobend+mydata->chips[0].numtobend;
			mydata->chips[i].disttobend=mydata->chips[i].disttobend+mydata->chips[0].disttobend;
		}
	}

}

//Initfunc 
void init(cJSON *msg,mystatus *needdata){
    cJSON *msg_data,*race_data,*track_data,*pieces_data,*lanes_data,*startingPoint_data,*raceSession_data;
	cJSON *tmp,*tmpchild;
	int i,j,tmpnum;
	//int lanemem[200];//1:straight 2:right 3:left
	//printf("Init!\n");

    msg_data = cJSON_GetObjectItem(msg, "data");
	race_data = cJSON_GetObjectItem(msg_data, "race");
	track_data = cJSON_GetObjectItem(race_data, "track");
	pieces_data = cJSON_GetObjectItem(track_data, "pieces");
	lanes_data = cJSON_GetObjectItem(track_data, "lanes");
	startingPoint_data = cJSON_GetObjectItem(track_data, "startingPoint");
	raceSession_data = cJSON_GetObjectItem(race_data,"raceSession");
	
	//puts(cJSON_Print(pieces_data));
	
	//Initialize
	needdata->turboflag=0;
	needdata->turbonum=0;
	needdata->Oldpos.pieceIndex=0;
	needdata->Oldpos.inPieceDistance=0;
	needdata->Nowpos.pieceIndex=0;
	needdata->Nowpos.inPieceDistance=0;
	needdata->Nowvelocity=0;
	needdata->Oldvelocity=0;
	needdata->OldAngle=0;
	needdata->NowAngle=0;
	needdata->onswitch=0;
	needdata->inif=0;
	needdata->nowlap=1;
	needdata->chips[0].disttobend=0;
	needdata->chips[0].numtobend=0;
	//Piece analyzer
	needdata->piecenum=cJSON_GetArraySize(pieces_data);
	for(i=0;i<needdata->piecenum;i++){
		//pick piecedata
		tmp=cJSON_GetArrayItem(pieces_data,i);
		tmpnum=cJSON_GetArraySize(tmp);
		//ini
		needdata->chips[i].chipflag=0;
		needdata->chips[i].switchflag=0;
		needdata->chips[i].shouldswitch=0;
		needdata->chips[i].length=0;
		needdata->chips[i].radius=0;
		needdata->chips[i].angle=0;

		//Straight or Bend Analyze
		if((tmpchild=cJSON_GetObjectItem(tmp,"length"))!=NULL){ //Straight
			needdata->chips[i].chipflag=1;
			needdata->chips[i].length=atof(cJSON_Print(tmpchild));
			if(tmpnum==2){//2?A^?C(?c,
				needdata->chips[i].switchflag=1;
			}

		}else{//Bend
			tmpchild=cJSON_GetObjectItem(tmp,"radius");
			needdata->chips[i].radius=atof(cJSON_Print(tmpchild));
			tmpchild=cJSON_GetObjectItem(tmp,"angle");
			needdata->chips[i].angle=atof(cJSON_Print(tmpchild));
			needdata->chips[i].length=fabs(2*needdata->chips[i].radius*M_PI*needdata->chips[i].angle/360.0);
			if(0<needdata->chips[i].angle){
				needdata->chips[i].chipflag=2;
			}else{
				needdata->chips[i].chipflag=3;
			}
			if(180<needdata->chips[i].radius){//not bend
				needdata->chips[i].chipflag=4;
			}

			if(tmpnum==3){
				needdata->chips[i].switchflag=1;
			}

		}
		//printf("chip[%d]  chipf:%d len:%f rad:%f ang:%f switch:%d\n",
		//	i,needdata->chips[i].chipflag,needdata->chips[i].length,needdata->chips[i].radius,needdata->chips[i].angle,needdata->chips[i].switchflag);
	}

	//Lane analyze
	needdata->lanenum=cJSON_GetArraySize(lanes_data);
	for(i=0;i<needdata->lanenum;i++){
		tmp=cJSON_GetArrayItem(lanes_data,i);
		tmpchild=cJSON_GetObjectItem(tmp,"distanceFromCenter");
		needdata->lanedistfromcenter[i]=tmpchild->valueint;
	}
	//laps
	needdata->lapnum=cJSON_GetObjectItem(raceSession_data,"laps")->valueint;

	//switch analyze
	switch_deside(needdata);

}

void init2(mystatus* mydata){
	int i;

	for(i=0;i<mydata->piecenum-2;i++){
		if(mydata->chips[i].chipflag==4 && mydata->chips[i+1].chipflag==4 && mydata->chips[i+2].chipflag!=1){
			if(0<mydata->chips[i].angle){
				mydata->chips[i].chipflag=2;
				mydata->chips[i].radius=130;
			}else{
				mydata->chips[i].chipflag=3;
				mydata->chips[i].radius=130;
			}
		}

	}
	stage_analyze(mydata);

}

cJSON *myplan(cJSON *msg,mystatus *mydata){
	cJSON *msg_data,*car_data,*angle_data,*id_data,*name_data,*position_data,*lane_data,*s_data,*e_data;
	cJSON *tmpjson;
	int carnum,i,nextpiece,tmp;
	double bendlength;
	
	msg_data = cJSON_GetObjectItem(msg, "data");
	carnum=cJSON_GetArraySize(msg_data);

	//Reset NowPos
	mydata->Oldpos.pieceIndex=mydata->Nowpos.pieceIndex;
	mydata->Oldpos.inPieceDistance=mydata->Nowpos.inPieceDistance;
	mydata->OldAngle=mydata->NowAngle;
	if(0<mydata->Nowvelocity){
		mydata->Oldvelocity=mydata->Nowvelocity;
	}
	for(i=0;i<carnum;i++){
		car_data=cJSON_GetArrayItem(msg_data,i);
		id_data=cJSON_GetObjectItem(car_data, "id");
		name_data=cJSON_GetObjectItem(id_data, "name");
		if(!strcmp(name_data->valuestring,"PnumaSON")){//myname
			angle_data=cJSON_GetObjectItem(car_data, "angle");
			position_data=cJSON_GetObjectItem(car_data, "piecePosition");
			tmpjson=cJSON_GetObjectItem(position_data, "pieceIndex");
			mydata->Nowpos.pieceIndex=tmpjson->valueint;
			tmpjson=cJSON_GetObjectItem(position_data, "inPieceDistance");
			mydata->Nowpos.inPieceDistance=tmpjson->valuedouble;
			lane_data=cJSON_GetObjectItem(position_data, "lane");
			s_data=cJSON_GetObjectItem(lane_data,"startLaneIndex");
			e_data=cJSON_GetObjectItem(lane_data,"endLaneIndex");
		}
	}
	
	//Velocity
	if(mydata->Nowpos.pieceIndex==mydata->Oldpos.pieceIndex){//SameIndex
		mydata->Nowvelocity=mydata->Nowpos.inPieceDistance-mydata->Oldpos.inPieceDistance;
	}else{
		if(1<mydata->chips[mydata->Oldpos.pieceIndex].chipflag){
			//bendlength=fabs(2*(mydata->chips[mydata->Oldpos.pieceIndex].radius-mydata->lanedistfromcenter[s_data->valueint])*M_PI*mydata->chips[mydata->Oldpos.pieceIndex].angle/360.0);
			if(mydata->onswitch==1 && mydata->chips[mydata->Oldpos.pieceIndex].switchflag==1){
				//if(mydata->chips[mydata->Oldpos.pieceIndex].shouldswitch==1){
				//	tmp=1;
				//}else if(mydata->chips[mydata->Oldpos.pieceIndex].shouldswitch==2){
				//	tmp=-1;
				//}
				//if(tmp+s_data->valueint<0 || mydata->lanenum <=s_data->valueint){
				//	tmp=0;
				//}
				//bendlength=mydata->chips[mydata->Oldpos.pieceIndex].length
				//		   -mydata->lanedistfromcenter[s_data->valueint]*M_PI*mydata->chips[mydata->Oldpos.pieceIndex].angle/360.0
				//		   -mydata->lanedistfromcenter[s_data->valueint+tmp]*M_PI*mydata->chips[mydata->Oldpos.pieceIndex].angle/360.0;
				//mydata->Nowvelocity=mydata->Nowpos.inPieceDistance+(bendlength-mydata->Oldpos.inPieceDistance);
				mydata->Nowvelocity=-1;
			}else{
				bendlength=mydata->chips[mydata->Oldpos.pieceIndex].length
						   -2*mydata->lanedistfromcenter[s_data->valueint]*M_PI*mydata->chips[mydata->Oldpos.pieceIndex].angle/360.0;
				mydata->Nowvelocity=mydata->Nowpos.inPieceDistance+(bendlength-mydata->Oldpos.inPieceDistance);
			}
		}else{
			if(mydata->onswitch==1 && mydata->chips[mydata->Oldpos.pieceIndex].switchflag==1){
				//if(mydata->chips[mydata->Oldpos.pieceIndex].shouldswitch==1){
				//	tmp=-1;
				//}else if(mydata->chips[mydata->Oldpos.pieceIndex].shouldswitch==2){
				//	tmp=1;
				//}
				//if(tmp+s_data->valueint<0 || mydata->lanenum <=s_data->valueint){
				//	tmp=0;
				//}
				//bendlength=sqrtf(mydata->chips[mydata->Oldpos.pieceIndex].length*mydata->chips[mydata->Oldpos.pieceIndex].length
				//				 + (mydata->lanedistfromcenter[s_data->valueint]-mydata->lanedistfromcenter[s_data->valueint+tmp])*(mydata->lanedistfromcenter[s_data->valueint]-mydata->lanedistfromcenter[s_data->valueint+tmp]));

				//mydata->Nowvelocity=mydata->Nowpos.inPieceDistance+(bendlength-mydata->Oldpos.inPieceDistance);
				mydata->Nowvelocity=-1;

			}else{
				mydata->Nowvelocity=mydata->Nowpos.inPieceDistance+(mydata->chips[mydata->Oldpos.pieceIndex].length-mydata->Oldpos.inPieceDistance);
			}
		}
	}

	//print
	//printf("velocity %f  angle %f\n",mydata->Nowvelocity,angle_data->valuedouble);
	//if(mydata->Nowvelocity!=-1 &&(mydata->Nowvelocity<0 || 10<mydata->Nowvelocity) ){
	//	puts(cJSON_Print(msg));
	//	printf("%f %f %f %d\n",mydata->Nowpos.inPieceDistance,bendlength,mydata->Oldpos.inPieceDistance,mydata->Oldpos.pieceIndex);
	//}

	mydata->NowAngle=fabs(angle_data->valuedouble);
	if(mydata->Oldpos.pieceIndex==mydata->piecenum-1 && mydata->Nowpos.pieceIndex==0){
		mydata->nowlap++;
	}
	nextpiece=(mydata->Nowpos.pieceIndex+1)%mydata->piecenum;

	//switch set
	if(mydata->chips[nextpiece].switchflag==1 && mydata->chips[nextpiece].shouldswitch!=0 && mydata->onswitch==0){
		//printf("Switch!\n");
		mydata->onswitch=1;
		if(mydata->chips[nextpiece].shouldswitch==1){
			return make_msg("switchLane", cJSON_CreateString("Right"));
		}else{
			return make_msg("switchLane", cJSON_CreateString("Left"));
		}
	}
	if(0<=mydata->Nowpos.pieceIndex-1 && mydata->chips[mydata->Nowpos.pieceIndex-1].switchflag==1 && mydata->onswitch==1){
		mydata->onswitch=0;
	}


	//throttle set
	//if(7<mydata->Oldvelocity){
	//	mydata->Nowvelocity=8;
	//	return throttle_msg(0.0);
	//}
	//return throttle_msg(1.0);


	//nearest bend check
	int bendnum=(mydata->Nowpos.pieceIndex+mydata->chips[mydata->Nowpos.pieceIndex].numtobend)%mydata->piecenum;
	double bendrad=mydata->chips[bendnum].radius;
	double target_velocity,target_throttle;
	double needdist,offset=0;
	

	for(i=1;i<3;i++){
		if(mydata->chips[(bendnum+i)%mydata->piecenum].numtobend==0){
			if(bendrad > mydata->chips[(bendnum+i)%mydata->piecenum].radius){
				bendrad=mydata->chips[(bendnum+i)%mydata->piecenum].radius;
			}
		}else{
			break;
		}
	}
	//bendrad=100 <-> throttle(0.6) <-> Vel 6.2		bendrad=50 <-> throttle(0.45) <-> vel4.3
	// Bend to target_vel
	target_velocity=0.035*bendrad+2.6;
	if(mydata->Nowvelocity==-1){
		mydata->Nowvelocity=mydata->Oldvelocity;
	}
	needdist=(2*mydata->Nowvelocity-target_velocity)*5*target_velocity;
	if(15<mydata->Nowvelocity){
		needdist=needdist*1.2;
	}
	else if(10<mydata->Nowvelocity){
		needdist=needdist*1.1;
	}

	if(45<fabs(angle_data->valuedouble)&& mydata->Nowvelocity+0.5>target_velocity){
		return throttle_msg(0.0);
	}

	target_throttle=target_velocity/10.0*1.1;

	if(mydata->chips[mydata->Nowpos.pieceIndex].numtobend!=0){//Now not Bend
		if((mydata->Nowvelocity+1)*30+(2*(mydata->Nowvelocity+5)-target_velocity)*5*target_velocity  <mydata->chips[mydata->Nowpos.pieceIndex].disttobend-mydata->Nowpos.inPieceDistance && 0<mydata->turbonum && mydata->turboflag==0 && fabs(angle_data->valuedouble)<35 && mydata->NowAngle < mydata->OldAngle){
			mydata->turboflag=1;
			mydata->turbonum--;
			//printf("calcneeddist %f \n Nowhav %f\n",(mydata->Nowvelocity+1)*30+(2*(mydata->Nowvelocity+5)-target_velocity)*5*target_velocity,mydata->chips[mydata->Nowpos.pieceIndex].disttobend-mydata->Nowpos.inPieceDistance);

			//printf("turbo ON!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
			return turbo_msg();
		}
		else if(mydata->nowlap==mydata->lapnum && mydata->piecenum-1 < mydata->Nowpos.pieceIndex+mydata->chips[mydata->Nowpos.pieceIndex].numtobend  ){
			if((mydata->NowAngle < mydata->OldAngle && fabs(angle_data->valuedouble)<40)&& 0<mydata->turbonum && mydata->turboflag==0 && mydata->NowAngle < mydata->OldAngle+3){
				mydata->turboflag=1;
				mydata->turbonum--;
				//printf("calcneeddist %f \n Nowhav %f\n",(mydata->Nowvelocity+1)*30+(2*(mydata->Nowvelocity+5)-target_velocity)*5*target_velocity,mydata->chips[mydata->Nowpos.pieceIndex].disttobend-mydata->Nowpos.inPieceDistance);
				//printf("turbo ON!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
				return turbo_msg();
			}else if(mydata->NowAngle < mydata->OldAngle || fabs(angle_data->valuedouble)<10){
				//printf("throttele: 1.0\n");
				return throttle_msg(1.0);
			}
		}

		if(needdist < mydata->chips[mydata->Nowpos.pieceIndex].disttobend-mydata->Nowpos.inPieceDistance || mydata->Nowvelocity<target_velocity){
			//printf("throttele: 1.0\n");
			return throttle_msg(1.0);
		}else{//target<Nowvel
			if(mydata->Nowvelocity<target_velocity+0.5){
				
				//printf("throttele: %f\n",target_throttle);
				return throttle_msg(target_throttle);
			}else{
				//printf("throttele: 0.0\n");
				return throttle_msg(0.0);
			}
		}
	}else{//Now Bend
		if(mydata->NowAngle < mydata->OldAngle && mydata->chips[nextpiece].numtobend!=0 && mydata->chips[(nextpiece+1)%mydata->piecenum].numtobend!=0){
			//printf("throttele: 1.0\n");
			return throttle_msg(1.0);
		}

		if(mydata->Nowvelocity<=target_velocity){
			if(fabs(angle_data->valuedouble)<30){
				//printf("throttele: 1.0\n");
				return throttle_msg(1.0);
			}

			//printf("throttele: %f\n",target_throttle);
			return throttle_msg(target_throttle);
		}else{//target<Nowvel
			if(fabs(angle_data->valuedouble)<30 && mydata->Nowvelocity<=target_velocity+0.5){
				//printf("throttele: %f\n",target_throttle);
				return throttle_msg(target_throttle);
			}

			if(mydata->chips[nextpiece].numtobend!=0 && fabs(angle_data->valuedouble)<40 - (100-mydata->chips[mydata->Nowpos.pieceIndex].radius)/10.0){
				//printf("throttele: %f\n",target_throttle);
				return throttle_msg(target_throttle);
			}
			//printf("throttele: 0.0\n");
			return throttle_msg(0.0);
		}
	}


	//if(mydata->chips[mydata->Nowpos.pieceIndex].chipflag==1 || mydata->chips[mydata->Nowpos.pieceIndex].chipflag==4){//now Straight
	//	if(mydata->chips[nextpiece].chipflag==1 || mydata->chips[nextpiece].chipflag==4){
	//		return throttle_msg(1.0);
	//	}else{
	//		return throttle_msg(0.3);
	//		if(mydata->chips[mydata->Nowpos.pieceIndex].length*0.8<mydata->Nowpos.inPieceDistance){
	//			return throttle_msg(0.3);
	//		}else{
	//			return throttle_msg(1.0);
	//		}
	//	}
	//}else{//now Bend
	//	return throttle_msg(0.3);
	//}
}

//print mydata
void printstatus(mystatus *pdata){
	int i;
	for(i=0;i<pdata->piecenum;i++){
		//printf("chip[%d]  chipf:%d len:%f rad:%f ang:%f switch:%d\n",i,pdata->chips[i].chipflag,
		//pdata->chips[i].length,pdata->chips[i].radius,pdata->chips[i].angle,pdata->chips[i].switchflag);
	}
}

void printdist(mystatus *pdata){
	int i;
	for(i=0;i<pdata->piecenum;i++){
		//printf("chip[%d]  chipf:%d len:%f numtobend:%d disttobend:%f\n",i,pdata->chips[i].chipflag,
		//	pdata->chips[i].length,pdata->chips[i].numtobend,pdata->chips[i].disttobend);
	}

}

int main(int argc, char *argv[])
{
    int sock;
    cJSON *json;
    mystatus mydata;

    if (argc != 5)
        error("Usage: bot host port botname botkey\n");

    sock = connect_to(argv[1], argv[2]);

    json = join_msg(argv[3], argv[4]);
	//json = CreateRace(argv[3], argv[4],"imola",1);
    write_msg(sock, json);
    cJSON_Delete(json);

    
    while ((json = read_msg(sock)) != NULL) {
        cJSON *msg, *msg_type,*msgtmp;
        char *msg_type_name;




        msg_type = cJSON_GetObjectItem(json, "msgType");
        if (msg_type == NULL)
            error("missing msgType field");

        msg_type_name = msg_type->valuestring;
		if(mydata.inif==0){
			mydata.inif=1;
			init2(&mydata);
			msg=throttle_msg(1.0);
		}
        else if (!strcmp("carPositions", msg_type_name)) {
			msg = myplan(json,&mydata);
            //msg = throttle_msg(0.6);
        }
		else if(!strcmp("gameInit", msg_type_name)){
			init(json,&mydata);//my init all
			//printstatus(&mydata);
			//printdist(&mydata);
			msg = ping_msg(); 
		}
		else if(!strcmp("turboAvailable",msg_type_name)){
			mydata.turbonum++;
			//printf("turbo full!\n");
			msg = ping_msg();
		}
		else if(!strcmp("turboEnd",msg_type_name)){
			msg=cJSON_GetObjectItem(json, "data");
			msgtmp=cJSON_GetObjectItem(msg, "name");
			if(!strcmp(msgtmp->valuestring,"PnumaSON")){
				//printf("turboEnd!\n");
				mydata.turboflag=0;
			}
			msg = ping_msg();
		
		}
		else {
            log_message(msg_type_name, json);
            msg = ping_msg();
        }

        write_msg(sock, msg);

        cJSON_Delete(msg);
        cJSON_Delete(json);
    }

    return 0;
}

static cJSON *ping_msg()
{
    return make_msg("ping", cJSON_CreateString("ping"));
}

static cJSON *join_msg(char *bot_name, char *bot_key)
{
    cJSON *data = cJSON_CreateObject();
    cJSON_AddStringToObject(data, "name", bot_name);
    cJSON_AddStringToObject(data, "key", bot_key);

    return make_msg("join", data);
}

static cJSON *throttle_msg(double throttle)
{
    return make_msg("throttle", cJSON_CreateNumber(throttle));
}

static cJSON *make_msg(char *type, cJSON *data)
{
    cJSON *json = cJSON_CreateObject();
    cJSON_AddStringToObject(json, "msgType", type);
    cJSON_AddItemToObject(json, "data", data);
    return json;
}

static cJSON *read_msg(int fd)
{
    int bufsz, readsz;
    char *readp, *buf;
    cJSON *json = NULL;

    bufsz = 16;
    readsz = 0;
    readp = buf = malloc(bufsz * sizeof(char));

    while (read(fd, readp, 1) > 0) {
        if (*readp == '\n')
            break;

        readp++;
        if (++readsz == bufsz) {
            buf = realloc(buf, bufsz *= 2);
            readp = buf + readsz;
        }
    }

    if (readsz > 0) {
        *readp = '\0';
        json = cJSON_Parse(buf);
        if (json == NULL)
            error("malformed JSON(%s): %s", cJSON_GetErrorPtr(), buf);
    }
    free(buf);
    return json;
}

static void write_msg(int fd, cJSON *msg)
{
    char nl = '\n';
    char *msg_str;

    msg_str = cJSON_PrintUnformatted(msg);

    write(fd, msg_str, strlen(msg_str));
    write(fd, &nl, 1);

    free(msg_str);
}
